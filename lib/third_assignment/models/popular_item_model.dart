class PopularItemModel {
  final String? popularThing;
  final String? locatedAt;
  final String? image;

  PopularItemModel({
    required this.popularThing,
    required this.locatedAt,
    required this.image,
  });
}

final popularList = [
  PopularItemModel(
      popularThing: 'Place of Culture and science',
      locatedAt: 'Gdansk',
      image:
          'https://cdn.pixabay.com/photo/2017/04/26/21/56/culture-2263891_1280.jpg'),
  PopularItemModel(
      popularThing: 'Second World War Museum',
      locatedAt: 'Warsaw',
      image:
          'https://cdn.pixabay.com/photo/2016/07/05/18/08/slave-quarters-1499121_1280.jpg'),
  PopularItemModel(
      popularThing: 'Royal Castle',
      locatedAt: 'Gulmarg',
      image:
          'https://cdn.pixabay.com/photo/2017/06/19/15/47/drottningholm-palace-2419776_1280.jpg'),
  PopularItemModel(
      popularThing: 'Dimitris vet Villas',
      locatedAt: 'Noida',
      image:
          'https://cdn.pixabay.com/photo/2017/04/10/22/28/residence-2219972_1280.jpg'),
  PopularItemModel(
      popularThing: 'Night light music',
      locatedAt: 'Gurugram',
      image:
          'https://cdn.pixabay.com/photo/2017/07/21/23/57/concert-2527495_1280.jpg'),
  PopularItemModel(
      popularThing: 'Troll, Gnome, Mythical creatures image',
      locatedAt: 'Zargdbjsd',
      image:
          'https://cdn.pixabay.com/photo/2015/05/26/21/32/troll-785557_1280.jpg'),
  PopularItemModel(
      popularThing: 'Second World War Museum',
      locatedAt: 'Warsaw',
      image:
          'https://cdn.pixabay.com/photo/2016/07/05/18/08/slave-quarters-1499121_1280.jpg'),
  PopularItemModel(
      popularThing: 'Royal Castle',
      locatedAt: 'Gulmarg',
      image:
          'https://cdn.pixabay.com/photo/2017/06/19/15/47/drottningholm-palace-2419776_1280.jpg'),
  PopularItemModel(
      popularThing: 'Dimitris vet Villas',
      locatedAt: 'Noida',
      image:
          'https://cdn.pixabay.com/photo/2017/04/10/22/28/residence-2219972_1280.jpg'),
  PopularItemModel(
      popularThing: 'Night light music',
      locatedAt: 'Gurugram',
      image:
          'https://cdn.pixabay.com/photo/2017/07/21/23/57/concert-2527495_1280.jpg'),
];
