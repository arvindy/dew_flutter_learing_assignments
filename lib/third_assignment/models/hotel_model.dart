class HotelModel {
  final String? image;
  bool isFab;
  final String? name;
  final String? locatedAt;
  final double? averageRating;
  final int totalRatingCount;
  final String? distance;
  final String? price;

  HotelModel({
    required this.image,
    this.isFab = false,
    required this.name,
    required this.locatedAt,
    required this.averageRating,
    required this.totalRatingCount,
    required this.distance,
    required this.price,
  });
}

final hotelList = [
  HotelModel(
      image:
          'https://cdn.pixabay.com/photo/2012/11/21/10/24/building-66789_1280.jpg',
      isFab: false,
      name: 'Polonia Palace Hotel',
      locatedAt: 'Wola Warsaw',
      averageRating: 4.0,
      totalRatingCount: 188,
      distance: '1',
      price: '25'),
  HotelModel(
      image:
          'https://cdn.pixabay.com/photo/2016/11/18/22/21/restaurant-1837150_1280.jpg',
      isFab: true,
      name: 'Sofitel Warsaw Hotel',
      locatedAt: 'Mokotov Warsaw',
      averageRating: 3.0,
      totalRatingCount: 96,
      distance: '1',
      price: '25'),
  HotelModel(
      image:
          'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/27/19/7a/67/hotel-facade-night-view.jpg?w=1400&h=-1&s=1',
      isFab: false,
      name: 'Mount magnolia boutique',
      locatedAt: 'Pelling-Rimbi Rd, Pelling',
      averageRating: 4.0,
      totalRatingCount: 432,
      distance: '12.8',
      price: '6'),
  HotelModel(
      image:
          'https://cdn.pixabay.com/photo/2012/11/21/10/24/building-66789_1280.jpg',
      isFab: false,
      name: 'Polonia Palace Hotel',
      locatedAt: 'Wola Warsaw',
      averageRating: 4.0,
      totalRatingCount: 188,
      distance: '1',
      price: '25'),
  HotelModel(
      image:
          'https://cdn.pixabay.com/photo/2016/11/18/22/21/restaurant-1837150_1280.jpg',
      isFab: true,
      name: 'Sofitel Warsaw Hotel',
      locatedAt: 'Mokotov Warsaw',
      averageRating: 3.0,
      totalRatingCount: 96,
      distance: '1',
      price: '25'),
  HotelModel(
      image:
          'https://dynamic-media-cdn.tripadvisor.com/media/photo-o/27/19/7a/67/hotel-facade-night-view.jpg?w=1400&h=-1&s=1',
      isFab: false,
      name: 'Mount magnolia boutique ',
      locatedAt: 'Pelling-Rimbi Rd, Pelling',
      averageRating: 4,
      totalRatingCount: 42,
      distance: '12',
      price: '6'),
];
