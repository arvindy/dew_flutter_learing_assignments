class CategoryModel {
  final String? categoryName;
  final String? categoryImage;

  CategoryModel({
    required this.categoryName,
    required this.categoryImage,
  });
}

final categories = [
  CategoryModel(
      categoryName: 'Hotels',
      categoryImage:
          'https://cdn.pixabay.com/photo/2017/01/14/12/48/hotel-1979406_1280.jpg'),
  CategoryModel(
      categoryName: 'Flights',
      categoryImage:
          'https://cdn.pixabay.com/photo/2020/05/23/04/11/transport-5207942_1280.jpg'),
  CategoryModel(
      categoryName: 'Activities',
      categoryImage:
          'https://cdn.pixabay.com/photo/2016/08/01/20/13/girl-1561989_1280.jpg'),
  CategoryModel(
      categoryName: 'Restaurant',
      categoryImage:
          'https://cdn.pixabay.com/photo/2014/09/17/20/26/restaurant-449952_1280.jpg'),
  CategoryModel(
      categoryName: 'Trains',
      categoryImage:
          'https://cdn.pixabay.com/photo/2016/03/05/23/02/blur-1239439_1280.jpg')
];
