class StoryModel {
  final String? profileImage;
  final String? storyImage;

  StoryModel({
    required this.profileImage,
    required this.storyImage,
  });
}

final stories = [
  StoryModel(
      profileImage:
          'https://cdn.pixabay.com/photo/2018/01/06/09/25/hijab-3064633_1280.jpg',
      storyImage:
          'https://cdn.pixabay.com/photo/2022/01/16/19/01/candle-6942931_1280.jpg'),
  StoryModel(
      profileImage:
          'https://cdn.pixabay.com/photo/2020/04/08/08/47/winter-5016422_1280.jpg',
      storyImage:
          'https://cdn.pixabay.com/photo/2015/05/07/19/49/girl-757030_1280.jpg'),
  StoryModel(
      profileImage:
          'https://cdn.pixabay.com/photo/2015/06/22/08/37/children-817365_1280.jpg',
      storyImage:
          'https://cdn.pixabay.com/photo/2016/05/20/11/02/family-1404825_1280.jpg'),
  StoryModel(
      profileImage:
          'https://cdn.pixabay.com/photo/2015/11/06/11/32/girl-1026246_1280.jpg',
      storyImage:
          'https://cdn.pixabay.com/photo/2017/01/19/05/29/night-1991590_1280.jpg'),
  StoryModel(
      profileImage:
      'https://cdn.pixabay.com/photo/2020/04/08/08/47/winter-5016422_1280.jpg',
      storyImage:
      'https://cdn.pixabay.com/photo/2015/05/07/19/49/girl-757030_1280.jpg'),
  StoryModel(
      profileImage:
      'https://cdn.pixabay.com/photo/2015/06/22/08/37/children-817365_1280.jpg',
      storyImage:
      'https://cdn.pixabay.com/photo/2016/05/20/11/02/family-1404825_1280.jpg'),
];
