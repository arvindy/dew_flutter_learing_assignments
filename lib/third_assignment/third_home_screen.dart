import 'package:dew_flutter_learing_assignments/third_assignment/models/category_model.dart';
import 'package:dew_flutter_learing_assignments/third_assignment/models/hotel_model.dart';
import 'package:dew_flutter_learing_assignments/third_assignment/models/story_model.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import '../constants.dart';
import 'models/popular_item_model.dart';

class ThirdAssignmentHomeScreen extends StatelessWidget {
  static String id = 'ThirdAssignmentHomeScreen';

  const ThirdAssignmentHomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.grey[200],
        body: const SafeArea(
            child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              HeaderWidget(),
              CategoryListWidget(),
              SearchWidget(),
              StoryListWidget(),
              MostPopularWidget(),
              CityBestPlaces(),
              HotelsWidget(),
            ],
          ),
        )));
  }
}

class HeaderWidget extends StatelessWidget {
  const HeaderWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return const Padding(
      padding: EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        bottom: 12.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            'Explore',
            style: kTextStyleExLargeBoldBlack,
          ),
          Badge(
            backgroundColor: Colors.blue,
            alignment: AlignmentDirectional.topEnd,
            smallSize: 10.0,
            child: Icon(Icons.notifications_none_outlined, size: 32),
          )
        ],
      ),
    );
  }
}

class CategoryListWidget extends StatelessWidget {
  const CategoryListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 96.0,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        scrollDirection: Axis.horizontal,
        itemCount: categories.length,
        itemBuilder: (BuildContext context, int index) {
          var category = categories[index];
          return CategoryWidget(categoryModel: category);
        },
        separatorBuilder: (BuildContext context, int index) =>
            const SizedBox(width: 20, height: 20),
      ),
    );
  }
}

class CategoryWidget extends StatelessWidget {
  final CategoryModel categoryModel;

  const CategoryWidget({super.key, required this.categoryModel});

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        CircleAvatar(
            radius: 32,
            backgroundImage: NetworkImage(categoryModel.categoryImage!)),
        Padding(
          padding: const EdgeInsets.only(
            top: 4.0,
          ),
          child: Text(
            textAlign: TextAlign.center,
            categoryModel.categoryName!,
            style: kTextStyleSmallRegularBlack,
          ),
        )
      ],
    );
  }
}

class SearchWidget extends StatelessWidget {
  const SearchWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.all(8.0),
      padding: const EdgeInsets.symmetric(horizontal: 12),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12.0),
      ),
      child: const Row(
        children: [
          Icon(
            Icons.search,
            color: Colors.grey,
          ),
          SizedBox(
            width: 8,
          ),
          Expanded(
            child: TextField(
              style: kTextStyleMediumRegularBlack,
              decoration: InputDecoration(
                hintText: 'Search City',
                hintStyle: kTextStyleMediumRegularGray,
                border: InputBorder.none,
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class StoryListWidget extends StatelessWidget {
  const StoryListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: SizedBox(
        height: 160.0,
        child: ListView.separated(
          padding: const EdgeInsets.symmetric(horizontal: 16.0),
          scrollDirection: Axis.horizontal,
          itemCount: stories.length,
          itemBuilder: (BuildContext context, int index) {
            var story = stories[index];
            if (index == 0) {
              return DefaultStoryWidget(storyModel: story);
            } else {
              return StoryWidget(storyModel: story);
            }
          },
          separatorBuilder: (BuildContext context, int index) =>
              const SizedBox(width: 16, height: 16),
        ),
      ),
    );
  }
}

class DefaultStoryWidget extends StatelessWidget {
  final StoryModel storyModel;

  const DefaultStoryWidget({super.key, required this.storyModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 190,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        border: Border.all(
          color: Colors.grey,
          width: 1,
        ),
        color: Colors.white,
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(8.0),
        child: Stack(
          children: [
            Image.network(
              storyModel.profileImage!,
              width: 90,
              height: 100,
              fit: BoxFit.cover,
            ),
            Positioned(
                top: 84,
                left: 29,
                right: 29,
                child: SizedBox(
                  width: 32,
                  height: 32,
                  child: FloatingActionButton(
                    onPressed: () {},
                    backgroundColor: Colors.blue,
                    child: const Icon(
                      Icons.add,
                      color: Colors.white,
                    ),
                  ),
                )),
            const Positioned(
              bottom: 8,
              top: 116,
              left: 8,
              right: 8,
              child: Center(
                  // alignment: Alignment.center,
                  child: Text(
                'Create\nStory',
                maxLines: 2,
                textAlign: TextAlign.center,
                style: kTextStylexSmallRegularBlack,
              )),
            ),
          ],
        ),
      ),
    );
  }
}

class StoryWidget extends StatelessWidget {
  final StoryModel storyModel;

  const StoryWidget({super.key, required this.storyModel});

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            width: 90, height: 160,
            storyModel.storyImage!,
            fit: BoxFit.fill, // use this
          ),
        ),
        Positioned(
            left: 8.0,
            top: 8.0,
            child: CircleAvatar(
              backgroundColor: Colors.white,
              radius: 17,
              child: CircleAvatar(
                  radius: 16,
                  backgroundImage: NetworkImage(storyModel.profileImage!)),
            ))
      ],
    );
  }
}

class MostPopularWidget extends StatelessWidget {
  const MostPopularWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Text(
              'Most Popular Now',
              style: kTextStyleMediumBoldBlack,
            ),
          ),
          SizedBox(
            height: 360.0,
            child: GridView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              scrollDirection: Axis.horizontal,
              itemCount: popularList.length,
              gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3,
                crossAxisSpacing: 4,
                mainAxisSpacing: 16,
                childAspectRatio: 1 / 2.6,
              ),
              itemBuilder: (BuildContext context, int index) {
                var model = popularList[index];
                return PopularWidget(dataModel: model);
              },
            ),
          ),
        ],
      ),
    );
  }
}

class CityBestPlaces extends StatelessWidget {
  const CityBestPlaces({super.key});

  @override
  Widget build(BuildContext context) {
    var heightOfWidget = MediaQuery.of(context).size.width * 1.25;

    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Stack(
        children: [
          Container(
            height: heightOfWidget,
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: NetworkImage(
                      'https://cdn.pixabay.com/photo/2022/07/22/19/54/gdansk-7338840_1280.jpg'),
                  fit: BoxFit.cover),
            ),
          ),
          Container(
            height: heightOfWidget,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black.withOpacity(0),
                  Colors.black.withOpacity(1),
                ],
              ),
            ),
          ),
          Positioned(
              width: MediaQuery.of(context).size.width,
              bottom: 32,
              child: const Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text(
                      'Poland',
                      style: kTextStyleMediumRegularWhite,
                    ),
                  ),
                  SizedBox(
                    width: 8.0,
                    height: 8.0,
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 16.0),
                    child: Text(
                      'A spellbinding cities where cultures collide',
                      style: kTextStyleSmallRegularWhite,
                    ),
                  ),
                  SizedBox(
                    width: 8.0,
                    height: 8.0,
                  ),
                  BestPlacesListWidget(),
                ],
              ))
        ],
      ),
    );
  }
}

class BestPlacesListWidget extends StatelessWidget {
  const BestPlacesListWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 120,
      child: ListView.separated(
        padding: const EdgeInsets.symmetric(horizontal: 16.0),
        scrollDirection: Axis.horizontal,
        itemCount: stories.length,
        itemBuilder: (BuildContext context, int index) {
          var model = popularList[index];
          return BestPlaceWidget(dataModel: model);
        },
        separatorBuilder: (BuildContext context, int index) =>
            const SizedBox(width: 12, height: 12),
      ),
    );
  }
}

class BestPlaceWidget extends StatelessWidget {
  final PopularItemModel dataModel;

  const BestPlaceWidget({super.key, required this.dataModel});

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        ClipRRect(
          borderRadius: BorderRadius.circular(8),
          child: Image.network(
            width: 160, height: 90,
            dataModel.image!,
            fit: BoxFit.cover, // use this
          ),
        ),
        const SizedBox(
          width: 8,
          height: 8,
        ),
        Text(
          dataModel.locatedAt ?? '',
          style: kTextStyleSmallBoldWhite,
        )
      ],
    );
  }
}

class HotelsWidget extends StatelessWidget {
  const HotelsWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 16.0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Hotels',
                  style: kTextStyleMediumBoldBlack,
                ),
                Text(
                  'See All',
                  style: kTextStyleSmallBoldBlue,
                ),
              ],
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).size.width * .75 + 16,
            child: ListView.separated(
              shrinkWrap: true,
              padding:
                  const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8),
              scrollDirection: Axis.horizontal,
              itemCount: stories.length,
              itemBuilder: (BuildContext context, int index) {
                var hotel = hotelList[index];
                return HotelCard(
                  hotelModel: hotel,
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const SizedBox(width: 16, height: 16),
            ),
          ),
        ],
      ),
    );
  }
}

class PopularWidget extends StatelessWidget {
  final PopularItemModel dataModel;

  const PopularWidget({super.key, required this.dataModel});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 16.0, bottom: 16.0, right: 16.0),
          child: AspectRatio(
            aspectRatio: 3 / 2,
            child: ClipRRect(
              borderRadius: BorderRadius.circular(8),
              child: Image.network(
                dataModel.image!,
                fit: BoxFit.cover, // use thi s
              ),
            ),
          ),
        ),
        const Text(
          '1',
          style: kTextStyleLargeBoldGray,
        ),
        const SizedBox(
          width: 8.0,
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        dataModel.popularThing ?? '',
                        style: kTextStyleMediumRegularBlack,
                        maxLines: 2,
                      ),
                      Text(
                        dataModel.locatedAt ?? '',
                        style: kTextStyleSmallRegularBlack,
                      ),
                    ],
                  ),
                ),
              ),
              Container(color: Colors.grey.shade400, height: 1)
            ],
          ),
        ),
      ],
    );
  }
}

class HotelCard extends StatelessWidget {
  final HotelModel hotelModel;

  const HotelCard({super.key, required this.hotelModel});

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    return SizedBox(
      width: screenWidth * 0.75,
      child: Card(
        clipBehavior: Clip.hardEdge,
        elevation: 4,
        shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12.0)),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Stack(
              children: [
                AspectRatio(
                  aspectRatio: 16 / 9,
                  child: Image.network(
                    hotelModel.image!,
                    fit: BoxFit.cover,
                  ),
                ),
                Positioned(
                    right: 18.0,
                    top: 18.0,
                    child: hotelModel.isFab
                        ? const Icon(
                            Icons.favorite,
                            color: Colors.white,
                          )
                        : const Icon(
                            Icons.favorite_border,
                            color: Colors.white,
                          ))
              ],
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        hotelModel.name ?? '',
                        style: kTextStyleMediumBoldBlack,
                      ),
                      Text(
                        hotelModel.locatedAt ?? '',
                        style: kTextStyleSmallRegularBlack,
                      ),
                      Wrap(
                        direction: Axis.horizontal,
                        spacing: 8.0,
                        children: [
                          RatingBar.builder(
                            initialRating: hotelModel.averageRating!,
                            minRating: 1,
                            direction: Axis.horizontal,
                            itemSize: 18.0,
                            itemCount: 5,
                            itemPadding: const EdgeInsets.only(right: 1.0),
                            itemBuilder: (context, _) => const Icon(
                              Icons.star,
                              color: Colors.blue,
                            ),
                            onRatingUpdate: (rating) {},
                          ),
                          Text(
                            '${hotelModel.averageRating ?? 0} (${hotelModel.totalRatingCount ?? 0})',
                            style: TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 14),
                          ),
                        ],
                      ),
                      Text(
                        '${hotelModel.distance ?? 0} Kilometer away',
                        style: kTextStyleSmallRegularGray,
                      ),
                    ],
                  ),
                  Container(
                    decoration: BoxDecoration(
                      color: Colors.black12,
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10, horizontal: 8),
                      child: RichText(
                        text: TextSpan(
                          text: '',
                          style: DefaultTextStyle.of(context).style,
                          children: <TextSpan>[
                            TextSpan(
                                text: '\$${hotelModel.price ?? 0}',
                                style: kTextStyleSmallBoldBlack),
                            const TextSpan(
                                text: ' night',
                                style: kTextStyleSmallRegularBlack),
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
