import 'package:dew_flutter_learing_assignments/second_assignment/models/card_option_model.dart';
import 'package:dew_flutter_learing_assignments/second_assignment/models/country_currency_model.dart';
import 'package:dew_flutter_learing_assignments/second_assignment/models/favourite_people_model.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';

class DewPayScreen extends StatelessWidget {
  static String id = 'DewPayScreen';

  DewPayScreen({super.key});

  final countryCurrencyList = [
    CountryCurrencyModel(
      name: 'India',
      flag:
          'https://www.pngimages.in/uploads/png-webp/2022/2022-August/India_Flag_png_images_download.webp',
      currency: 'INR',
    ),
    CountryCurrencyModel(
      name: 'United States',
      flag:
          'https://freepngimg.com/thumb/usa/172995-usa-free-download-png-hd.png',
      currency: 'USD',
    ),
    CountryCurrencyModel(
      name: 'Canada',
      flag:
          'https://www.seekpng.com/png/full/338-3380353_png-50-px-canada-flag-icon-png.png',
      currency: 'CAD',
    ),
    CountryCurrencyModel(
        name: 'Australia',
        flag:
            'https://freepngimg.com/thumb/australia/9-2-australia-flag-png.png',
        currency: 'AUD'),
  ];

  final cardList = [
    CardOptionModel(
        cardIcon: Icons.payment,
        title: 'Pay someone',
        description: 'To wallet, bank or mobile no',
        cardColorHexa: '0xFFf5f5fe'),
    CardOptionModel(
        cardIcon: Icons.money,
        title: 'Request money',
        description: 'Request money from OroboPay users',
        cardColorHexa: '0xFFdae9da'),
    CardOptionModel(
        cardIcon: Icons.add_business,
        title: 'Buy airtime',
        description: 'Top-up or send airtime across Africa',
        cardColorHexa: '0xFFfffaf0'),
    CardOptionModel(
        cardIcon: Icons.paypal,
        title: 'Pay bill',
        description: 'Zero transaction fees when you pay',
        cardColorHexa: '0xFFf3f4f6'),
  ];

  final favList = [
    FavouritePeopleModel(name: 'Add', profileUrl: '', countryUrl: ''),
    FavouritePeopleModel(
      name: 'Robin Uthappa ',
      profileUrl:
          'http://cricket.upcomingwiki.com/public/images/gallery/94098.png',
      countryUrl:
          'https://www.pngimages.in/uploads/png-webp/2022/2022-August/India_Flag_png_images_download.webp',
    ),
    FavouritePeopleModel(
      name: 'Devid Warner',
      profileUrl: '',
      countryUrl:
          'https://freepngimg.com/thumb/australia/9-2-australia-flag-png.png',
    ),
    FavouritePeopleModel(
      name: 'Nitish',
      profileUrl: '',
      countryUrl:
          'https://www.pngimages.in/uploads/png-webp/2022/2022-August/India_Flag_png_images_download.webp',
    ),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.only(left: 24.0, right: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  'Hi Dewers,',
                  style: kTextStyleLargeRegularGray,
                ),
                Badge(
                  backgroundColor: Colors.red,
                  alignment: AlignmentDirectional.topEnd,
                  smallSize: 10.0,
                  child: Icon(Icons.notifications_none_outlined, size: 32),
                )
              ],
            ),
            const SizedBox(
              height: 8.0,
            ),
            const Text(
              '1,234.00',
              style: kTextStyleSize48BoldBlack,
            ),
            CountryCurrencyDropDown(
              ccList: countryCurrencyList,
            ),
            const SizedBox(
              height: 36.0,
            ),
            const Text('Here are some things you can do',
                style: kTextStyleLargeRegularGray),
            const SizedBox(
              height: 24.0,
            ),
            GridView.count(
              physics: const NeverScrollableScrollPhysics(),
              shrinkWrap: true,
              mainAxisSpacing: 16.0,
              crossAxisSpacing: 16.0,
              // Create a grid with 2 columns. If you change the scrollDirection to
              // horizontal, this produces 2 rows.
              crossAxisCount: 2,
              children: List.generate(cardList.length, (index) {
                return CardWidget(
                  cardOptionModel: cardList[index],
                );
              }),
            ),
            const SizedBox(
              height: 24.0,
            ),
            const Text('Your favourites people',
                style: kTextStyleLargeRegularGray),
            const SizedBox(
              height: 24.0,
            ),
            SizedBox(
              height: 128.0,
              child: ListView.separated(
                scrollDirection: Axis.horizontal,
                itemCount: favList.length,
                itemBuilder: (BuildContext context, int index) {
                  var fabModel = favList[index];
                  if (index == 0) {
                    return AddFavPeopleWidget(
                      favouritePeopleModel: fabModel,
                    );
                  } else {
                    return FavPeopleWidget(favouritePeopleModel: fabModel);
                  }
                },
                separatorBuilder: (BuildContext context, int index) =>
                    const SizedBox(width: 20, height: 20),
              ),
            ),
          ],
        ),
      ),
    )));
  }
}

class CardWidget extends StatelessWidget {
  final CardOptionModel cardOptionModel;

  const CardWidget({super.key, required this.cardOptionModel});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.greenAccent,
        borderRadius: BorderRadius.all(Radius.circular(16.0)),
      ),
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Icon(
              cardOptionModel.cardIcon,
              size: 36.0,
            ),
            const SizedBox(
              height: 8.0,
            ),
            Text(cardOptionModel.title, style: kTextStyleMediumBoldBlack),
            const SizedBox(
              height: 8.0,
            ),
            Text(
              maxLines: 2,
              cardOptionModel.description,
              style: kTextStyleMediumRegularGray,
            ),
          ],
        ),
      ),
    );
  }
}

class AddFavPeopleWidget extends StatelessWidget {
  final FavouritePeopleModel favouritePeopleModel;

  const AddFavPeopleWidget({super.key, required this.favouritePeopleModel});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 84.0,
      child: Column(
        children: [
          Container(
              width: 84.0,
              height: 84.0,
              decoration: const BoxDecoration(
                shape: BoxShape.circle,
                color: Colors.cyanAccent,
              ),
              child: const Icon(Icons.add, size: 48, color: Colors.blueGrey)),
          Padding(
            padding: const EdgeInsets.only(
              left: 8.0,
              top: 8.0,
              right: 8.0,
            ),
            child: Text(
              textAlign: TextAlign.center,
              favouritePeopleModel.name,
              style: kTextStyleMediumRegularEllipsisBlack,
            ),
          )
        ],
      ),
    );
  }
}

class FavPeopleWidget extends StatelessWidget {
  final FavouritePeopleModel favouritePeopleModel;

  const FavPeopleWidget({super.key, required this.favouritePeopleModel});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 84.0,
      child: Column(
        children: [
          Stack(
            children: [
              favouritePeopleModel.profileUrl.isNotEmpty
                  ? CircleAvatar(
                      radius: 42,
                      backgroundImage:
                          NetworkImage(favouritePeopleModel.profileUrl))
                  : Container(
                      width: 84.0,
                      height: 84.0,
                      decoration: const BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.blue,
                      ),
                      child: Center(
                          child: Text(
                        favouritePeopleModel.getFistCharsOfFirstLastName(),
                        style: kTextStyleExLargeBoldWhite,
                      ))),
              if (favouritePeopleModel.countryUrl.isNotEmpty)
                Positioned(
                  right: 0,
                  bottom: 0,
                  child: CircleAvatar(
                    radius: 14,
                    backgroundColor: Colors.white,
                    child: CircleAvatar(
                      radius: 12,
                      backgroundImage:
                          NetworkImage(favouritePeopleModel.countryUrl),
                    ),
                  ),
                )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 8.0,
              top: 8.0,
              right: 8.0,
            ),
            child: Text(
              textAlign: TextAlign.center,
              favouritePeopleModel.name,
              style: kTextStyleMediumRegularEllipsisBlack,
            ),
          )
        ],
      ),
    );
  }
}

class CountryCurrencyDropDown extends StatefulWidget {
  final List<CountryCurrencyModel> ccList;

  const CountryCurrencyDropDown({super.key, required this.ccList});

  @override
  State<CountryCurrencyDropDown> createState() =>
      _CountryCurrencyDropDownState();
}

class _CountryCurrencyDropDownState extends State<CountryCurrencyDropDown> {
  CountryCurrencyModel? selectedItem;

  @override
  Widget build(BuildContext context) {
    return DropdownButton(
      dropdownColor: Colors.white,
      underline: const SizedBox(),
      icon: const Padding(
        padding: EdgeInsets.only(left: 8.0),
        child: Icon(
          Icons.keyboard_arrow_down,
          color: Colors.grey,
        ),
      ),
      value: selectedItem ?? widget.ccList.first,
      items: widget.ccList
          .map(
            (value) => DropdownMenuItem(
              value: value,
              child: Row(
                children: [
                  ClipOval(
                    child: Image.network(
                      value.flag,
                      width: 24,
                      height: 24,
                      fit: BoxFit.cover,
                    ),
                  ),
                  const Padding(padding: EdgeInsets.only(left: 10)),
                  Text(
                    value.currency,
                    style: kTextStyleLargeRegularGray,
                  ),
                ],
              ),
            ),
          )
          .toList(),
      onChanged: (value) {
        setState(() {
          selectedItem = value;
        });
      },
    );
  }
}
