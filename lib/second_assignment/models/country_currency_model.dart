class CountryCurrencyModel {
  final String name;
  final String flag;
  final String currency;

  CountryCurrencyModel(
      {required this.name,
      required this.flag,
      required this.currency});
}
