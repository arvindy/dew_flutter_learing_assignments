class FavouritePeopleModel {
  final String name;
  final String profileUrl;
  final String countryUrl;

  FavouritePeopleModel(
      {required this.name, required this.profileUrl, required this.countryUrl});

  String getFistCharsOfFirstLastName() {
    var fLChar = '';
    if (name.isNotEmpty) {
      var splitArray = name.trim().split(' ');

      if (splitArray.length > 1) {
        fLChar = splitArray.first.toUpperCase()[0] +
            splitArray.last.toUpperCase()[0];
      } else {
        fLChar = splitArray.first.toUpperCase()[0];
      }
    }
    return fLChar;
  }
}
