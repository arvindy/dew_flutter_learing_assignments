import 'package:flutter/cupertino.dart';

class CardOptionModel {
  final IconData cardIcon;
  final String title;
  final String description;
  final String cardColorHexa;

  CardOptionModel(
      {required this.cardIcon,
      required this.title,
      required this.description,
      required this.cardColorHexa});
}
