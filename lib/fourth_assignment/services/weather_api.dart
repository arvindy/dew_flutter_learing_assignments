import 'dart:convert';
import 'dart:io';
import 'package:dew_flutter_learing_assignments/fourth_assignment/models/weather_response.dart';
import 'package:dew_flutter_learing_assignments/fourth_assignment/services/location.dart';

import 'networking.dart';

const String apiKey = "862d4cd190c4469bf97b8c44347f069d";

const String openWeatherBaseUrl = "https://api.openweathermap.org/data/2.5";

//https://api.openweathermap.org/data/2.5/forecast?
// lat=28.608950&lon=77.308258&appid=862d4cd190c4469bf97b8c44347f069d&units=metric

class WeatherApi {
  Future<WeatherResponse> getLocationWeather() async {
    Location location = Location();
    var position = await location.getCurrentLocation();

    print('lat${position.latitude}, lon${position.longitude}, ');

    NetworkHelper networkHelper = NetworkHelper(
        '$openWeatherBaseUrl/forecast?lat=${position.latitude}&lon=${position.longitude}&appid=${apiKey}&units=metric');

    var response = await networkHelper.getData();

    print('response ## $response');

    return WeatherResponse.fromJson(
        response as Map<String, dynamic>);
  }

  static String getWeatherIcon(int condition) {
    if (condition < 300) {
      return '🌩';
    } else if (condition < 400) {
      return '🌧';
    } else if (condition < 600) {
      return '☔️';
    } else if (condition < 700) {
      return '☃️';
    } else if (condition < 800) {
      return '🌫';
    } else if (condition == 800) {
      return '☀️';
    } else if (condition <= 804) {
      return '☁️';
    } else {
      return '🤷‍';
    }
  }

  String getMessage(int temp) {
    if (temp > 25) {
      return 'It\'s 🍦 time';
    } else if (temp > 20) {
      return 'Time for shorts and 👕';
    } else if (temp < 10) {
      return 'You\'ll need 🧣 and 🧤';
    } else {
      return 'Bring a 🧥 just in case';
    }
  }
}
