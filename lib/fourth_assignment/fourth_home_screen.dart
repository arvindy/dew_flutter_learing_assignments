import 'package:dew_flutter_learing_assignments/fourth_assignment/services/weather_api.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';

import '../constants.dart';
import 'models/weather_response.dart';

class FourthAssignmentScreen extends StatefulWidget {
  static String id = 'FourthAssignmentScreen';

  const FourthAssignmentScreen({super.key});

  @override
  State<FourthAssignmentScreen> createState() => _FourthAssignmentScreenState();
}

class _FourthAssignmentScreenState extends State<FourthAssignmentScreen> {
  final GlobalKey<ScaffoldState> _key = GlobalKey(); //
  bool isLoading = false; // C
  WeatherApi weatherApi = WeatherApi();
  WeatherResponse? weatherResponse;

  @override
  void initState() {
    super.initState();
    getWeatherData();
  }

  void getWeatherData() async {
    isLoading = true;
    setState(() {});

    weatherResponse = await weatherApi.getLocationWeather();

    isLoading = false;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          key: _key,
          drawer: Drawer(
            // Add a ListView to the drawer. This ensures the user can scroll
            // through the options in the drawer if there isn't enough vertical
            // space to fit everything.
            child: ListView(
              // Important: Remove any padding from the ListView.
              padding: EdgeInsets.zero,
              children: [
                const DrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.blue,
                  ),
                  child: Text('Welcome to weather'),
                ),
                ListTile(
                  title: const Text('Item 1'),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
                ListTile(
                  title: const Text('Item 2'),
                  onTap: () {
                    // Update the state of the app.
                    // ...
                  },
                ),
              ],
            ),
          ),
          backgroundColor: Colors.grey[200],
          body: SafeArea(
              child: isLoading
                  ? const Center(
                      child: SpinKitDoubleBounce(
                        color: Colors.white,
                        size: 100.0,
                      ),
                    )
                  : SingleChildScrollView(
                      child: Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.only(
                                left: 16, top: 12, bottom: 12, right: 16),
                            child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  InkWell(
                                      onTap: () {
                                        _key.currentState?.openDrawer();
                                      },
                                      child: const Icon(
                                        Icons.menu,
                                        size: 28,
                                      )),
                                  const Text(
                                    'Weather App',
                                    style: kTextStyleMediumRegularBlack,
                                  ),
                                  InkWell(
                                      onTap: () {
                                        getWeatherData();
                                      },
                                      child: const Icon(
                                        Icons.replay_circle_filled_sharp,
                                        size: 24,
                                      ))
                                ]),
                          ),
                          // Top
                          const SizedBox(
                            height: 24,
                            width: 24,
                          ),
                          Text(
                            weatherResponse?.city?.name ?? '',
                            style: const TextStyle(
                              color: Colors.black,
                              fontSize: 32,
                              fontWeight: FontWeight.normal,
                            ),
                          ),
                          const SizedBox(
                            height: 8,
                            width: 8,
                          ),
                          const Text(
                            'Today',
                            style: TextStyle(color: Colors.grey, fontSize: 24),
                          ),
                          const SizedBox(
                            height: 16,
                            width: 16,
                          ),
                          Text(
                              '${weatherResponse?.list?.first.main?.temp}°C' ??
                                  'Error°C',
                              style: const TextStyle(
                                color: Colors.blue,
                                fontSize: 54,
                                fontWeight: FontWeight.normal,
                              )),
                          const SizedBox(
                            height: 12,
                            width: 12,
                          ),
                          Container(
                            width: 100,
                            color: Colors.grey,
                            height: 1,
                          ),
                          const SizedBox(
                            height: 8,
                            width: 8,
                          ),
                          Text(
                            '${weatherResponse?.list?.first.weather?.first.main}' ??
                                'Error',
                            style: const TextStyle(
                                color: Colors.grey, fontSize: 24),
                          ),
                          const SizedBox(
                            height: 16,
                            width: 16,
                          ),
                          Text(
                            '${weatherResponse?.list?.first.main?.tempMin ?? 'min'}°C/${weatherResponse?.list?.first.main?.tempMax ?? 'max'}°C',
                            style: const TextStyle(
                                color: Colors.blue, fontSize: 24),
                          ),
                          if (weatherResponse?.list != null)
                            TodayForecast(weatherResponse: weatherResponse),
                          if (weatherResponse?.list != null)
                            SevenDaysForecast(weatherResponse: weatherResponse),
                        ],
                      ),
                    ))),
    );
  }
}

class SevenDaysForecast extends StatelessWidget {
  const SevenDaysForecast({super.key, required this.weatherResponse});

  final WeatherResponse? weatherResponse;

  @override
  Widget build(BuildContext context) {
    List<WeatherData> mList = weatherResponse?.list ?? [];
    Set<String> dateList = {};
    List<WeatherData> weekList = [];

    if (mList.isNotEmpty) {
      for (var element in mList) {
        dateList.add(element.dtTxt!.split(' ')[0]);
      }

      dateList.forEach((element) {
        print("Arv Text ${DateFormat('EEEE').format(DateTime.parse(element))}");
      });
      for (var element in mList) {
        if (weekList.isEmpty) {
          weekList.add(element);
        } else {
          var mDt = element.dtTxt!.split(' ')[0];
          var itemFound = false;
          for (var weekItem in weekList) {
            if (mDt == weekItem.dtTxt!.split(' ')[0]) {
              itemFound = true;
              break;
            }
          }
          if (!itemFound) {
            weekList.add(element);
          }
        }
      }

      print("${dateList.length} , final ${weekList.length}");
    }
    return Container(
      decoration: BoxDecoration(
          color: Colors.grey.withOpacity(.2),
          borderRadius: const BorderRadius.all(
            Radius.circular(8.0),
          )),
      margin: const EdgeInsets.only(top: 24, left: 16, right: 16, bottom: 24),
      padding: const EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Text(
            '7-day Forecast',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
          ),
          const SizedBox(
            height: 8,
            width: 8,
          ),
          const Divider(height: 1, color: Colors.grey),
          const SizedBox(
            height: 8,
            width: 8,
          ),
          ListView.separated(
              shrinkWrap: true,
              itemCount: weekList.length,
              physics: const NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
                WeatherData wData = weekList[index];
                return SevenDayForecastItem(
                  weatherData: wData,
                  isFirst: index == 0,
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(height: 1, color: Colors.grey)),
        ],
      ),
    );
  }
}

class SevenDayForecastItem extends StatelessWidget {
  final WeatherData weatherData;
  final bool isFirst;

  const SevenDayForecastItem(
      {super.key, required this.weatherData, required this.isFirst});

  @override
  Widget build(BuildContext context) {
    var day = DateFormat('EEEE')
        .format(DateTime.parse(weatherData.dtTxt!.split(' ')[0]));

    return Padding(
      padding: const EdgeInsets.only(top: 8.0, bottom: 8, left: 8, right: 8),
      child: Row(
        children: [
          Expanded(
            child: Text(
              isFirst ? 'Today' : day.substring(0, 3),
              style: kTextStyleLargeRegularBlack,
            ),
          ),
          Expanded(
            child: Text(
                WeatherApi.getWeatherIcon(weatherData.weather?[0].id ?? 10000),
                style: kTextStyleExLargeRegularWhite),
          ),
          Expanded(
            child: Text(
              '${weatherData.main?.tempMin?.toInt() ?? ''}°C',
              style: kTextStyleLargeRegularGray,
            ),
          ),
          Expanded(
            child: Center(
              child: Text('${weatherData.main?.tempMax?.toInt() ?? ''}°C',
                  style: kTextStyleLargeRegularBlack),
            ),
          ),
        ],
      ),
    );
  }
}

class TodayForecast extends StatefulWidget {
  const TodayForecast({super.key, required this.weatherResponse});

  final WeatherResponse? weatherResponse;

  @override
  State<TodayForecast> createState() => _TodayForecastState();
}

class _TodayForecastState extends State<TodayForecast> {
  @override
  Widget build(BuildContext context) {
    List<WeatherData> mList = widget.weatherResponse?.list ?? [];

    if (mList.isNotEmpty) {
      String todayDate =
          widget.weatherResponse?.list?.first.dtTxt?.split(' ')[0] ?? '';

      var timeHours = DateTime.now().toString();
      timeHours = timeHours.split(' ')[1].split(':')[0];

      List<WeatherData> todayForecast = mList.where((item) {
        return item.dtTxt?.split(' ')[0] == todayDate;
      }).toList();

      if (todayForecast.isNotEmpty) {
        return Container(
          width: double.infinity,
          decoration: BoxDecoration(
              color: Colors.grey.withOpacity(.2),
              borderRadius: const BorderRadius.all(
                Radius.circular(8.0),
              )),
          margin: const EdgeInsets.only(top: 8, left: 16, right: 16),
          padding: const EdgeInsets.all(8),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const Text(
                'Forecast for Today',
                style: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.bold,
                    fontSize: 16),
              ),
              const SizedBox(
                height: 8,
                width: 8,
              ),
              SizedBox(
                height: 230,
                child: ListView.separated(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: todayForecast.length,
                  itemBuilder: (BuildContext context, int index) {
                    WeatherData wData = todayForecast[index];
                    return TodayForecastItem(
                      weatherData: wData,
                      isFirst: index == 0,
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const SizedBox(width: 16, height: 16),
                ),
              ),
            ],
          ),
        );
      } else {
        return Container();
      }
    } else {
      return Container();
    }
  }
}

class TodayForecastItem extends StatelessWidget {
  final WeatherData weatherData;
  final bool isFirst;

  const TodayForecastItem(
      {super.key, required this.weatherData, required this.isFirst});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 64,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(
            isFirst
                ? 'Now'
                : (weatherData.dtTxt?.split(' ')[1].substring(0, 5) ?? ''),
            style: kTextStyleMediumRegularBlack,
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            WeatherApi.getWeatherIcon(weatherData.weather?[0].id ?? 10000),
            style: kTextStyleH4BoldBlack,
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            '${weatherData.main?.temp?.toInt().toString() ?? ''}°C',
            style: kTextStyleLargeRegularBlack,
          ),
          const SizedBox(
            height: 8,
          ),
          Image.asset(
            'assets/images/wind.png',
            width: 36,
            height: 36,
          ),
          Text(
            '${weatherData.wind?.speed.toString() ?? ''}km/h',
            style: kTextStyleSmallRegularGray,
          ),
          const SizedBox(
            height: 8,
          ),
          Image.asset(
            'assets/images/umbrella.png',
            width: 36,
            height: 36,
            color: Colors.blue,
          ),
          const SizedBox(
            height: 8,
          ),
          Text(
            '${(weatherData.pop ?? 0) * 100}%',
            style: kTextStyleSmallRegularBlue,
          ),
        ],
      ),
    );
  }
}
