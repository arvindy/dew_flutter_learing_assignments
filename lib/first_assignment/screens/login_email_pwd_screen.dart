import 'package:dew_flutter_learing_assignments/first_assignment/custom_widgets/custom_button.dart';
import 'package:dew_flutter_learing_assignments/first_assignment/screens/registration_screen.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../custom_widgets/custom_text_field.dart';

class LoginWithEmailPwdScreen extends StatelessWidget {
  static String id = 'LoginWithEmailPwdScreen';

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  LoginWithEmailPwdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/bgimg.jpeg'),
                  fit: BoxFit.cover),
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black.withOpacity(0.3),
                  Colors.black.withOpacity(0.9),
                ],
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  const Text(
                    "Welcome",
                    style: kTextStyleExLargeRegularWhite,
                  ),
                  const Text(
                    "Join Mr. BookWarm!",
                    style: kTextStyleSmallRegularWhite,
                  ),
                  const SizedBox(
                    height: 18.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'Email',
                      leftIcon: Icons.email,
                      teController: _emailController),
                  const SizedBox(
                    height: 16.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'Password',
                      leftIcon: Icons.key,
                      isPassword: true,
                      teController: _emailController),
                  const SizedBox(
                    height: 12.0,
                  ),
                  CustomButton(
                    text: 'Login',
                    backgroundColor: Colors.white,
                    onTapFun: () {},
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    "Don't have an account",
                    style: kTextStyleSmallRegularWhite,
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, RegistrationScreen.id);
                    },
                    child: const Text(
                      "Create account",
                      style: kTextStyleSmallBoldWhite,
                    ),
                  ),
                  const SizedBox(
                    height: 32.0,
                  )
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}
