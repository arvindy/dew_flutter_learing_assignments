import 'package:dew_flutter_learing_assignments/first_assignment/custom_widgets/custom_button.dart';
import 'package:dew_flutter_learing_assignments/first_assignment/screens/social_login_screen.dart';
import 'package:flutter/material.dart';

import '../../constants.dart';
import '../custom_widgets/custom_text_field.dart';

class RegistrationScreen extends StatelessWidget {
  static String id = 'RegistrationScreen';

  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();

  RegistrationScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          body: Stack(
        children: [
          Container(
            decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/bgimg.jpeg'),
                  fit: BoxFit.cover),
            ),
          ),
          Container(
            height: double.infinity,
            width: double.infinity,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [
                  Colors.black.withOpacity(0.3),
                  Colors.black.withOpacity(0.9),
                ],
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    "Welcome",
                    style: kTextStyleExLargeRegularWhite,
                  ),
                  const Text(
                    "Join Mr. BookWarm!",
                    style: kTextStyleSmallRegularWhite,
                  ),
                  const SizedBox(
                    height: 18.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'Email',
                      leftIcon: Icons.email,
                      teController: _emailController),
                  const SizedBox(
                    height: 16.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'City',
                      leftIcon: Icons.home,
                      teController: _emailController),
                  const SizedBox(
                    height: 16.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'Mobile Number',
                      leftIcon: Icons.phone,
                      teController: _emailController),
                  const SizedBox(
                    height: 16.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'Collage',
                      leftIcon: Icons.school,
                      teController: _emailController),
                  const SizedBox(
                    height: 16.0,
                  ),
                  CustomTextFieldWidget(
                      hintText: 'Password',
                      leftIcon: Icons.key,
                      isPassword: true,
                      teController: _emailController),
                  const SizedBox(
                    height: 12.0,
                  ),
                  CustomButton(
                    text: 'Register',
                    backgroundColor: Colors.white,
                    onTapFun: () {},
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                  const Text(
                    "Already have an account",
                    style: kTextStyleSmallRegularWhite,
                  ),
                  const SizedBox(
                    height: 16.0,
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.pushNamed(context, SocialLoginScreen.id);
                    },
                    child: const Text(
                      "Login",
                      style: kTextStyleSmallBoldWhite,
                    ),
                  ),
                  const SizedBox(
                    height: 32.0,
                  )
                ],
              ),
            ),
          ),
        ],
      )),
    );
  }
}
