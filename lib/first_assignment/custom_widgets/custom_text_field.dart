import 'package:flutter/material.dart';

import '../../constants.dart';

class CustomTextFieldWidget extends StatelessWidget {
  final String hintText;
  final IconData? leftIcon;
  final bool isPassword;
  final bool isReadOnly;
  final Function()? onTap;
  final TextEditingController? teController;

  const CustomTextFieldWidget(
      {super.key,
      required this.hintText,
      required this.leftIcon,
      this.isPassword = false,
      this.isReadOnly = false,
      this.teController,
      this.onTap});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 48.0,
      padding: const EdgeInsets.only(left: 16, right: 48),
      decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white,
            width: kBorderWidthSmall,
          ),
          borderRadius: BorderRadius.circular(24)),
      child: Row(
        children: [
          if (leftIcon != null)
            Icon(
              leftIcon,
              color: Colors.white,
              size: 24.0,
            ),
          const SizedBox(
            width: 16,
          ),
          Expanded(
              child: TextField(
            readOnly: isReadOnly,
            onTap: onTap,
            controller: teController,
            style: const TextStyle(
              color: Colors.white,
            ),
            textAlign: TextAlign.center,
            obscureText: isPassword,
            decoration: InputDecoration(
              hintText: hintText,
              hintStyle: const TextStyle(
                color: Colors.white70, // change hint color here
              ),
              border: InputBorder.none,
            ),
          ))
        ],
      ),
    );
  }
}
