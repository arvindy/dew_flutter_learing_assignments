import 'package:flutter/material.dart';

class CustomButton extends StatelessWidget {
  const CustomButton({super.key,
    required this.text,
    this.backgroundColor,
    this.textStyle,
    this.leftImageAsset,
    this.leftImageColor,
    this.onTapFun});

  final String text;
  final Color? backgroundColor;
  final TextStyle? textStyle;
  final AssetImage? leftImageAsset;
  final Color? leftImageColor;
  final Function()? onTapFun;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: onTapFun,
      child: Container(
        height: 48,
        padding: const EdgeInsets.only(left: 16, right: 40),
        decoration: BoxDecoration(
            color: backgroundColor ?? Colors.transparent,
            border: Border.all(
              color: backgroundColor ?? Colors.white,
              width: 2,
            ),
            borderRadius: BorderRadius.circular(25)),
        child: Row(
          children: [
            leftImageAsset != null
                ? Image(
              image: leftImageAsset!,
              height: 24,
              width: 24,
              color: leftImageColor,
            )
                : const SizedBox(
              width: 24.0,
              height: 24.0,
            ),
            const SizedBox(
              width: 16,
            ),
            Expanded(
                child: Text(
                  text,
                  textAlign: TextAlign.center,
                  style: textStyle,
                ))
          ],
        ),
      ),
    );
  }
}
