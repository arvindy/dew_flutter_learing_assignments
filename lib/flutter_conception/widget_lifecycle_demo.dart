import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/*
createState
    When we create a stateful widget, our framework calls a createState()
    method and it must be overridden.

initState()
    Flutter’s initState() method is the first method that is used while
    creating a stateful class, here we can initialize variables, data,
    properties, etc. for any widget.

didChangeDependencies()
    This method is called immediately after initState and when dependency
    of the State object changes via InheritedWidget.

build()
    The build method is used each time the widget is rebuilt. This can happen
    either after calling initState, didChangeDependencies, didUpdateWidget,
    or when the state is changed via a call to setState

didUpdateWidget()
    This method is called whenever the widget configuration changes. A typical
    case is when a parent passes some variable to the children() widget
    via the constructor.

setState()

deactivate()
    It is used when the state is removed from the tree but before the current
    frame change can be re-inserted into another part of the tree

dispose()
    We use this method when we remove permanently like should release resource created by an object like stop animation

 */



class LifeCycleTestMain extends StatelessWidget {
  static String id = 'LifeCycleTestMain';

  const LifeCycleTestMain({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const SafeArea(
      child: Scaffold(
          body: LifeCycleTest()),
    );
  }
}


class LifeCycleTest extends StatefulWidget {

  const LifeCycleTest({super.key});

  @override
  State<LifeCycleTest> createState() => _LifeCycleTestState();
}

class _LifeCycleTestState extends State<LifeCycleTest>
    with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    print("DewDemo initState");
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    print('DewDemo AppLifecycleState: $state');
  }

  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    print("DewDemo build");
    return Scaffold(
      appBar: AppBar(title: const Text("Flutter Widget Lifecycle")),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
            const SizedBox(
              height: 8.0,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }

  @override
  void deactivate() {
    print("DewDemo deactivate");
    super.deactivate();
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();
    print("DewDemo dispose");
  }
}
