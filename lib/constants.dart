import 'package:flutter/material.dart';

const kSendButtonTextStyle = TextStyle(
  color: Colors.lightBlueAccent,
  fontWeight: FontWeight.bold,
  fontSize: 18.0,
);

const kTextStylexSmallRegularBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 12);

const kTextStyleSize48BoldBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 48);

const kTextStyleH4BoldBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 32);

const kTextStyleExLargeRegularWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 20);

const kTextStyleExLargeBoldWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 20);

const kTextStyleExLargeBoldBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20);

const kTextStyleSmallRegularWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 14);

const kTextStyleSmallRegularBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 14);

const kTextStyleSmallRegularGray =
    TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 14);

const kTextStyleSmallBoldGray =
    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 14);

const kTextStyleSmallBoldBlue =
    TextStyle(color: Colors.blue, fontWeight: FontWeight.bold, fontSize: 14);

const kTextStyleSmallRegularBlue =
    TextStyle(color: Colors.blue, fontWeight: FontWeight.normal, fontSize: 14);

const kTextStyleSmallBoldBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 14);

const kTextStyleSmallBoldWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.bold, fontSize: 14);

const kTextStyleMediumRegularWhite =
    TextStyle(color: Colors.white, fontWeight: FontWeight.normal, fontSize: 16);

const kTextStyleMediumRegularGray =
    TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 16);

const kTextStyleMediumRegularBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 16);

const kTextStyleMediumRegularEllipsisBlack = TextStyle(
    overflow: TextOverflow.ellipsis,
    color: Colors.black,
    fontWeight: FontWeight.normal,
    fontSize: 16);

const kTextStyleMediumBoldBlack = TextStyle(
    overflow: TextOverflow.ellipsis,
    color: Colors.black,
    fontWeight: FontWeight.bold,
    fontSize: 16);

const kTextStyleMediumBoldGray =
    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 16);

const kTextStyleLargeRegularGray =
    TextStyle(color: Colors.grey, fontWeight: FontWeight.normal, fontSize: 18);

const kTextStyleLargeRegularBlack =
    TextStyle(color: Colors.black, fontWeight: FontWeight.normal, fontSize: 18);

const kTextStyleLargeBoldGray =
    TextStyle(color: Colors.grey, fontWeight: FontWeight.bold, fontSize: 18);

const kMessageContainerDecoration = BoxDecoration(
  border: Border(
    top: BorderSide(color: Colors.lightBlueAccent, width: 2.0),
  ),
);

const double kBorderWidthSmall = 2;
