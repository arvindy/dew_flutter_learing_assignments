import 'package:dew_flutter_learing_assignments/first_assignment/screens/login_email_pwd_screen.dart';
import 'package:dew_flutter_learing_assignments/second_assignment/screens/dew_pay_screen.dart';
import 'package:dew_flutter_learing_assignments/third_assignment/third_home_screen.dart';
import 'package:flutter/material.dart';

import 'first_assignment/screens/registration_screen.dart';
import 'first_assignment/screens/social_login_screen.dart';
import 'flutter_conception/widget_lifecycle_demo.dart';
import 'fourth_assignment/fourth_home_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: MyHomePage(),
      initialRoute: MyHomePage.id,
      routes: {
        MyHomePage.id: (context) => MyHomePage(),
        LoginWithEmailPwdScreen.id: (context) => LoginWithEmailPwdScreen(),
        SocialLoginScreen.id: (context) => SocialLoginScreen(),
        RegistrationScreen.id: (context) => RegistrationScreen(),
        DewPayScreen.id: (context) => DewPayScreen(),
        ThirdAssignmentHomeScreen.id: (context) => ThirdAssignmentHomeScreen(),
        FourthAssignmentScreen.id: (context) => FourthAssignmentScreen(),
        LifeCycleTestMain.id: (context) => LifeCycleTestMain(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({super.key});

  static String id = 'MyHomePage';
  final mList = [
    ListData('First Assignment',
        'It has login with email and password, login with social media and registration screens'),
    ListData('Second Assignment',
        'This example contains cards of payment and fav people'),
    ListData('Third Assignment',
        'This example contain your exploration places, restaurants and hotels. after second'),
    ListData('Fourth Assignment', 'Weather API integration'),
    ListData('Widget Lifecycle', 'Understand widget lifecycle exam.')
  ];

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Dew Flutter Learning Assignment'),
      ),
      body: ListView.separated(
          itemCount: widget.mList.length,
          itemBuilder: (context, index) {
            var listData = widget.mList[index];
            return ListTile(
              onTap: () {
                switch (index) {
                  case 0:
                    {
                      Navigator.pushNamed(context, SocialLoginScreen.id);
                    }
                    break;
                  case 1:
                    {
                      Navigator.pushNamed(context, DewPayScreen.id);
                    }
                    break;
                  case 2:
                    {
                      Navigator.pushNamed(
                          context, ThirdAssignmentHomeScreen.id);
                    }
                    break;
                  case 3:
                    {
                      Navigator.pushNamed(
                          context, FourthAssignmentScreen.id);
                    }
                    case 4:
                    {
                      Navigator.pushNamed(
                          context, LifeCycleTestMain.id);
                    }
                    break;
                }
              },
              title: Text(listData.title),
              subtitle: Text(
                listData.desc,
                style: const TextStyle(color: Colors.grey),
              ),
            );
          },
          separatorBuilder: (context, index) => const Divider(
              height: 1,
              color: Colors
                  .green)), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class ListData {
  final String title;
  final String desc;

  ListData(this.title, this.desc);
}
